import unittest
from supervisory import Supervisory
from should_dsl import should, should_not
import paho.mqtt.client as mqtt

class FakeEnvironment():
    TOPIC_IN = '/project/in'

    def __init__(self):
        self.mqtt_client = mqtt.Client() 
        self.mqtt_client.connect('localhost')

    def publish(self, message):
        self.mqtt_client.publish(
            self.TOPIC_IN,
            message
        )

    def set_river_high(self):
        self.publish('{river_high: true}')

    def set_river_low(self):
        self.publish('{river_high: false}')

    def set_tank_high(self):
        self.publish('{river_high: true}')


class SupervisoryTest(unittest.TestCase):

    def setUp(self):
        self.mqtt_host   = 'localhost'
        self.mqtt_input  = 'supervisory/in'
        self.mqtt_output = 'supervisory/out'
        self.mqtt_settings = dict(
            mqtt_host = self.mqtt_host,
            mqtt_input = self.mqtt_input,
            mqtt_output = self.mqtt_output
        )

    def test_supervisory_mqqt_settings_initialization(self):
        supervisory = Supervisory(**self.mqtt_settings)

        supervisory.mqtt_host |should| equal_to(self.mqtt_host)
        supervisory.mqtt_input |should| equal_to(self.mqtt_input)
        supervisory.mqtt_output |should| equal_to(self.mqtt_output)

    def test_mqqt_connection(self):
        supervisory = Supervisory(**self.mqtt_settings)

        supervisory.setup() |should| be(True)
