import paho.mqtt.client as mqtt
import ipdb


class Supervisory(mqtt.Client):

    
    def __init__(self, *args, **kwargs):
        self.mqtt_host   = kwargs.pop('mqtt_host', None)
        self.mqtt_input  = kwargs.pop('mqtt_input', None)
        self.mqtt_output = kwargs.pop('mqtt_output', None)
        super().__init__(*args, **kwargs)

    def setup(self):
        return self.connect(self.mqtt_host) == 0

    def on_connect(client, userdata, flags, rc):
        print("[STATUS] Conectado ao Broker. Resultado de conexao: "+str(rc))
        client.subscribe('/project/out')

    def on_message(client, userdata, msg):

        message = str(msg.payload) # converte a mensagem recebida
        print("[MSG RECEBIDA] Topico: "+msg.topic+" / Mensagem: "+ message)

    def on_disconnect(client, userdata,rc=0):
        client.loop_stop()

    def execute(self):
        pass


if __name__ == '__main__':
    sensor_nivel_critico_rio = int(input("Qual o status do Sensor de Nível Crítico do Rio? "))
    sensor_nivel_baixo_reservatorio = int(input("Qual o status do Sensor de Nível Baixo do Reservatório? "))
    sensor_nivel_alto_reservatorio = int(input("Qual o status do Sensor de Nível Alto do Reservatório? "))
    sensor_caixa_dagua = int(input("Qual o status do Sensor de Nível da Caixa D´água? "))
    bomba_rio = int
    bombra_caixa = int
    alarme = int

    if (sensor_nivel_alto_reservatorio == 0) and (sensor_nivel_critico_rio == 1):
        bomba_rio = 1
        print("Bomba do Rio - LIGADA - Status =", bomba_rio)
    else:
        bomba_rio = 0
        print("Bomba do Rio - DESLIGADA - Status =", bomba_rio)

    if (sensor_caixa_dagua == 0) and (sensor_nivel_baixo_reservatorio == 1):
        bombra_caixa = 1
        print("Bomba da Caixa - LIGADA - Status =", bombra_caixa)

    else:
        bombra_caixa = 0
        print("Bomba da Caixa - DESLIGADA - Status =", bombra_caixa)

    if sensor_nivel_critico_rio == 0:
        alarme = 1
        print("Alarme - LIGADO - Status =", alarme)

    else:
        alarme = 0
        print("Alarme - DESLIGADO - Status =", alarme)
